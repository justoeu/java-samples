package br.com.justoeu.samples.enumMap;

import org.junit.Test;

import java.util.*;
import java.util.AbstractMap.SimpleEntry;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class DayOfWeekTest {

    @Test
    public void test_should_verify_with_attributes_is_ok(){

        EnumMap<DayOfWeek, String> maps = new EnumMap<>(DayOfWeek.class);

        maps.put(DayOfWeek.SUNDAY, "Sunday Night Football");
        maps.put(DayOfWeek.WEDNESDAY, "NBA");

        assertThat(maps.size()).isEqualTo(2);
        assertThat(maps.get(DayOfWeek.SUNDAY)).isEqualTo("Sunday Night Football");
        assertThat(maps.get(DayOfWeek.WEDNESDAY)).isEqualTo("NBA");
        assertThat(maps.get(DayOfWeek.MONDAY)).isBlank();
    }

    @Test
    public void test_should_copy_map_to_enumMap(){

        Map<DayOfWeek,String> originalMap = new HashMap<>();
        originalMap.put(DayOfWeek.MONDAY, "Soccer");

        EnumMap<DayOfWeek,String> copyMap = new EnumMap(originalMap);

        assertThat(copyMap.size()).isEqualTo(1);
        assertThat(copyMap.get(DayOfWeek.MONDAY)).isEqualTo("Soccer");

    }

    @Test
    public void test_should_map_nothing(){

        EnumMap<DayOfWeek, String> maps = new EnumMap<>(DayOfWeek.class);

        maps.put(DayOfWeek.SUNDAY, "Sunday Night Football");
        maps.put(DayOfWeek.WEDNESDAY, "NBA");

        assertThat(maps.containsKey(DayOfWeek.SATURDAY)).isFalse();
        assertThat(maps.containsValue(null)).isFalse();
        maps.put(DayOfWeek.SATURDAY, null);
        assertThat(maps.containsKey(DayOfWeek.SATURDAY)).isTrue();
        assertThat(maps.containsValue(null)).isTrue();
    }

    @Test
    public void test_should_remove_elements(){

        EnumMap<DayOfWeek, String> maps = new EnumMap<>(DayOfWeek.class);

        maps.put(DayOfWeek.SUNDAY, "Sunday Night Football");
        maps.put(DayOfWeek.WEDNESDAY, "NBA");
        maps.put(DayOfWeek.MONDAY, "Soccer");

        assertThat(maps.remove(DayOfWeek.MONDAY, "Hiking")).isEqualTo(false);
        assertThat(maps.remove(DayOfWeek.MONDAY, "Soccer")).isEqualTo(true);

    }

    @Test
    public void test_should_veryfy_All_Elements(){

        EnumMap<DayOfWeek, String> maps = new EnumMap<>(DayOfWeek.class);

        maps.put(DayOfWeek.SUNDAY, "Sunday Night Football");
        maps.put(DayOfWeek.WEDNESDAY, "NBA");
        maps.put(DayOfWeek.MONDAY, "Soccer");

        Collection values = maps.values();
        assertThat(values).contains("Soccer", "NBA", "Sunday Night Football");
        assertThat(values).containsExactly("Soccer", "NBA", "Sunday Night Football");

    }


    @Test
    public void test_should_veryfy_element(){

        EnumMap<DayOfWeek, String> maps = new EnumMap<>(DayOfWeek.class);

        maps.put(DayOfWeek.SUNDAY, "Sunday Night Football");
        maps.put(DayOfWeek.WEDNESDAY, "NBA");
        maps.put(DayOfWeek.MONDAY, "Soccer");

        assertThat(maps.entrySet())
                .containsExactly(
                        new SimpleEntry(DayOfWeek.MONDAY, "Soccer"),
                        new SimpleEntry(DayOfWeek.WEDNESDAY, "NBA"),
                        new SimpleEntry(DayOfWeek.SUNDAY, "Sunday Night Football")

                );

    }

}